(define-package "ol-notmuch" "2.0.0" "links to notmuch messages"
  '((emacs "25.1")
    (compat "28.1.1.0")
    (notmuch "0.32")
    (org "9.4.5"))
  :url "https://git.sr.ht/~tarsius/ol-notmuch/")
